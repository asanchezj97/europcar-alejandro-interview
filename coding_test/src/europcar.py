import os
import json
import pandas as pd
from src.transform import Transform
import logging

log = logging.getLogger("logger")


class Europcar:
    """
    Methods:
        join_path() -> From a given folder, file name and format, return a string with the full path
        exists_path() -> Return true if a given path exists
        file_to_df() -> Generate a DF from a file in a given path and format
        df_to_file() -> Export a dataframe to a File with a given output path and format
        read_configuration() -> Read the configuration file from a given path and generate a json object
        run() -> From a given DF, apply transforms and finally export it into a file

    Attributes:
        config: Configuration file given from input

        source_path: Source dataset path
        source_dataset: Source dataset name
        source_format: Source dataset format

        sink_path: Output dataset path
        sink_dataset: Output dataset name
        sink_format: Output dataset format

        source_file_path: merged source file path
        output_file_path: merged sink file path

        df: dataset file converted into a dataframe
    """
    def __init__(self, input_path):
        self.config = self.__read_configuration(input_path)  # Read the configuration file into an object

        self.source_path = self.config["source"]["path"]
        self.source_dataset = self.config["source"]["dataset"]
        self.source_format = self.config["source"]["format"]
        self.sink_path = self.config["sink"]["path"]
        self.sink_dataset = self.config["sink"]["dataset"]
        self.sink_format = self.config["sink"]["format"]
        self.transforms = self.config["transforms"]

        self.source_file_path = self.join_path(self.source_path, self.source_dataset, self.source_format)
        self.output_file_path = self.join_path(self.sink_path, self.sink_dataset, self.sink_format)

        self.df = self.__file_to_df(self.source_file_path, self.source_format)

    @staticmethod
    def join_path(folder, file_name, file_format):
        return ".".join([(folder + file_name), file_format])

    @staticmethod
    def exists_path(path):
        if os.path.exists(path):
            return True
        else:
            return False

    def __file_to_df(self, file_path, file_format):
        try:
            log.info("Reading a file into a dataframe..")
            if self.exists_path(file_path):
                if file_format == "csv":
                    return pd.read_csv(file_path)
                elif file_format == "json":
                    return pd.read_json(file_path)
                elif file_format == "jsonl":
                    return pd.read_json(file_path, lines=True)
                elif file_format == "parquet":
                    return pd.read_parquet(file_path)
                else:
                    raise Exception("File format not allowed.")
            else:
                raise Exception("The dataset file could not be founded. Check the path.")
        except Exception as e:
            log.error(e)
            raise Exception(f"Unable to read the dataset file: {e}")

    def __df_to_file(self, df, output_path, output_format):

        try:
            log.info("Exporting DF to a file..")

            if not self.exists_path(self.sink_path):
                os.makedirs(self.sink_path)

            if output_format == "csv":
                return df.to_csv(output_path)
            elif output_format == "json":
                return df.to_json(output_path, orient="records")
            elif output_format == "parquet":
                return df.to_parquet(output_path)
            elif output_format == "jsonl":
                return df.to_json(output_path, orient="records", lines=True)
            else:
                raise Exception("Output format not allowed.")
        except Exception as e:
            log.error(e)
            raise Exception(f"Unable to write the file: {e}")

    def __read_configuration(self, path):
        try:
            log.info("Reading configuration file...")
            if self.exists_path(path):
                f = open(path)
                config_data = json.load(f)
                f.close()
                return config_data
            else:
                raise Exception("The configuration file could not be read. Check the path or format.")
        except Exception as e:
            log.error(e)
            raise Exception(f"Unable to read the config file: {e}")

    def run(self):
        transform = Transform(self.df, self.transforms)
        df = transform.run()

        self.__df_to_file(df, self.output_file_path, self.sink_format)
        return df
