import datetime as dt
import logging
from numpy import right_shift
import pandas as pd

log = logging.getLogger("logger")


class Transform:
    """
    Methods:
        compare_values -> Return 1 if two given values are the same, otherwise return 0
        str_to_datetime -> Return a datetime object from a given str formatted datetime
        compare_dates -> Given a date, compare it with today and return the time in between in years
        birthdate_to_age -> Given a date, it computes the age of a person and creates a new column with the result.
        hot_encoding -> Given a categorical column with n possible values, it replaces it with n binary columns.
                        For example, given the column “color” with 3 possible values: “blue”, “red” and “green”,
                        we will create 3 columns: “is_blue”, “is_red” and “is_green”,
                        that will be 1 or 0 depending of the value of the original column.
        fill_empty_values -> It replaces the empty values of a column with another value.
        run -> Execute the transformations based on the configuration file

    Attributes:
        df: A dataset
        transforms: Certain transformations to do against the dataset
    """
    def __init__(self, df, transforms):
        self.df = df
        self.transforms = transforms

    @staticmethod
    def compare_values(val1, val2):
        result = 1 if val1 == val2 else 0
        return result

    @staticmethod
    def str_to_datetime(str_datetime):
        return dt.datetime.strptime(str_datetime, '%Y-%M-%d')

    def __compare_dates(self, dob):
        dob = self.str_to_datetime(dob)
        today = dt.date.today()
        return today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day))

    def _birthdate_to_age(self, fields):
        try:
            for f in fields:
                field_to_convert = f["field"]
                new_field = f["new_field"]
                self.df[new_field] = self.df[field_to_convert].apply(lambda x: self.__compare_dates(x))
        except Exception as e:
            log.error(e)
            raise Exception(f"Error detected: birthday_to_age: {e}")

    def _hot_encoding(self, fields):
        try:
            
            for f in fields:
                dummies = pd.get_dummies(self.df[f])
                self.df = self.df.join(dummies)
        except Exception as e:
            log.error(e)
            raise Exception(f"Error detected: hot encoding: {e}")

    def _fill_empty_values(self, fields):
        const_values_empty = ["mean", "median", "mode"]
        try:
            for f in fields:
                field = f["field"]
                value = f["value"]
                if value in const_values_empty:
                    self.df[field] = self.df[field].fillna(self.df[field].apply(value), inplace=False)           
                else:
                    self.df[field] = self.df[field].fillna(value)
        except Exception as e:
            log.error(e)
            raise Exception(f"Error detected: fill empty values: {e}")

    def run(self):
        log.info("Applying transforms...")
        
        for t in self.transforms:
            fields = t["fields"]
            transform = t["transform"]
            f = f"self._{transform}({fields})"
            eval(f)
        
        return self.df
            