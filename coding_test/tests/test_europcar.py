from src.europcar import Europcar
from src.transform import Transform
import unittest
import datetime as dt

class TestEuropcar(unittest.TestCase):
    def test_join_path(self):
        folder = "inputs/"
        name = "clean_bookings"
        format = "json"
        actual = Europcar.join_path(folder=folder, file_name=name, file_format=format)
        expected = "inputs/clean_bookings.json"
        self.assertEqual(actual, expected)

    def test_exists_path(self):
        path="tests/config.json"
        actual = Europcar.exists_path(path=path)
        expected = True
        self.assertEqual(actual, expected)

class TestTransform(unittest.TestCase):
    def test_compare_values(self):
        v1 = 0
        v2 = 1
        actual = Transform.compare_values(v1, v2)
        expected = 0
        self.assertEqual(actual, expected)

        v1 = 1
        v2 = 1
        actual = Transform.compare_values(v1, v2)
        expected = 1
        self.assertEqual(actual, expected)
    
    def test_str_to_datetime(self):
        str_datetime = "2021-01-02"
        actual = Transform.str_to_datetime(str_datetime=str_datetime)
        expected = dt.datetime.strptime(str_datetime, "%Y-%M-%d")

        self.assertEqual(actual, expected)
