from src.europcar import Europcar
from src.logger import Logger
import argparse
from time import sleep
log = Logger()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Europcar Interview")
    parser.add_argument("-i", "--input", help="input dataset", required=True)

    try:
        args = parser.parse_args()
        input_file = args.input
    except argparse.ArgumentError as e:
        log.error(e)
        raise argparse.ArgumentTypeError("Error getting arguments.")

    try:
        log.info("Starting process..")

        e = Europcar(input_file)
        df = e.run()

        log.info(f'Result: \n {df.to_string()}')
        log.info("Process ended.")
    except Exception as e:
        log.error(f"Error: {e}")
