# Europcar Interview

## Coding Test

- Docker compose (v3.8)
```
cd coding_test
docker-compose run --rm --entrypoint "python -m unittest tests/test_europcar.py" europcar_alejandro
docker-compose up --build
```

- Docker
```
cd coding_test
docker build -t europcaralejandro .
docker run europcaralejandro python -m unittest tests/test_europcar.py
docker run -v ${PWD}/logs:/app/logs -v ${PWD}/outputs:/app/outputs europcaralejandro 
```


The output will be saved to coding_test/logs and coding_test/outputs to make it more readable


## Architecture Test

Link to PDF:

[Cloud Architecture](https://gitlab.com/asanchezj97/europcar-alejandro/-/blob/main/Cloud%20Architecture%20-%20Connected%20Cars.pdf)


## Integration Test

- Basic mode
```
cd integration_test
bash ./integration_test.sh --config_file_path=./config_file.json --credentials_file='{"mycredsfile":"mycredsvalue"}' --execution_mode=basic
```

- Complex mode
```
cd integration_test
bash ./integration_test.sh --config_file_path=./config_file.json --credentials_file='{"mycredsfile":"mycredsvalue"}' --execution_mode=complex
```
