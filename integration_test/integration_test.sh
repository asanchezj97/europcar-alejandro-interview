#!/bin/bash

BASIC_MODE="basic"
COMPLEX_MODE="complex"


function write_file(){
  # $1 -> Data
  # $2 -> Path

  echo "$1" > "$2"
}

for i in "$@"; do
  case $i in
    -cfp=*|--config_file_path=*)
      CONFIG_FILE_PATH="${i#*=}"
      shift
      ;;
    -cf=*|--credentials_file=*)
      CREDENTIALS_FILE="${i#*=}"
      shift
      ;;
    -em=*|--execution_mode=*)
      EXECUTION_MODE="${i#*=}"
      shift
      ;;    
    *)      
      ;;
  esac
done

if [ "${EXECUTION_MODE,,}" = "${BASIC_MODE}" ] ; then
  echo "Mode selected: Basic"
  python ./event_consumer.py --runner DataflowRunner --config "${CONFIG_FILE_PATH}" --environment dev --integration-test true > /dev/null &
elif [ "${EXECUTION_MODE,,}" = "${COMPLEX_MODE}" ] ; then
  echo "Mode selected: Complex"  
  output_path="credentials.json"

  write_file "${CREDENTIALS_FILE}" "${output_path}" #1. Print the output of the CREDENTIALS_FILE to a file called credentials.json
  python ./integration_test.py --config "${CONFIG_FILE_PATH}" --environment dev --mode "${EXECUTION_MODE}" #2. Take the parameters passed as input and log them in a file.
else
  echo "Mode not valid"
fi
