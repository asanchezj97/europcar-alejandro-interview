"""
This file should take the values passed from integration_test.sh and log them in some file.
"""

import argparse
import json


def save_json_file(path, data):
    with open(path, 'w') as fp:
        json.dump(data, fp)


parser = argparse.ArgumentParser(description="Europcar Interview")
parser.add_argument("-config", "--config", help="Config file", required=True)
parser.add_argument("-environment", "--environment", help="Environment", required=True)
parser.add_argument("-mode", "--mode", help="Execution mode", required=True)

try:
    args = parser.parse_args()
    config = args.config
    environment = args.environment
    mode = args.mode
    
except argparse.ArgumentError as e:
    raise argparse.ArgumentTypeError(f"Error getting arguments: {e}")

if __name__ == "__main__":
    path = "./log.json"
    log = {"config": config, "env": environment, "mode": mode}

    save_json_file(path, log)
